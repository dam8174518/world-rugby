<?php
if (!isset($_SESSION)) session_start();

if (!isset($_SESSION['logged_in']) || $_SESSION['logged_in'] !== true) {
    header("Location: login-form.php");
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" type="image/x-icon" href="../../media/logos/favicon.ico" />
    <link rel="stylesheet" href="estilos/general.css" />
    <link rel="stylesheet" href="estilos/header.css" />
    <link rel="stylesheet" href="estilos/headerMediaQuery.css" />
    <link rel="stylesheet" href="estilos/footer.css" />
    <link rel="stylesheet" href="estilos/footerMediaQuery.css" />
    <link rel="stylesheet" href="estilos/equipos/squad.css" />
    <script src="https://kit.fontawesome.com/6cb64a97a2.js" crossorigin="anonymous"></script>
    <title>Resultados &#x21aa; worldrugby.org</title>
</head>

<body>

    <?php include('php/templates/header.php');  ?>
    <script src="scripts/header.js"></script>


    <div class="top-section-container">
        <div class="text-container">
            <h1 class="title">Super Rugby</h1>
            <p class="sub-title">Tabla de jugadores.</p>
        </div>
    </div>

    <div class="select-outer-container">

        <div class="select-container">
            <select class="select-box" name="selectSeason" id="selectSeason">
                <?php

                $xml = simplexml_load_file("data/temporadas.xml");
                foreach ($xml->temporada as $season) {
                    echo "<option value='" . $season['id'] . "'>" . $season['id'] . "</option>";
                }
                ?>
            </select>
        </div>
        <div class="select-box-icon-container">
            <i class="fa-solid fa-caret-down"></i>
        </div>
    </div>

    <div class="select-outer-container-team">

        <div class="select-container">
            <select class="select-box" name="selectTeam" id="selectTeam">

            </select>
        </div>
        <div class="select-box-icon-container">
            <i class="fa-solid fa-caret-down"></i>
        </div>
    </div>

    <div id="result"></div>

    <div class="squad-cards-container">
    </div>


    <script>
        document.addEventListener('DOMContentLoaded', function() {
            loadTeams();

            document.getElementById('selectSeason').addEventListener('change', function() {
                loadTeams();
            });

            document.getElementById('selectTeam').addEventListener('change', function() {
                loadPlayers();
            });
        });

        
        function loadTeams() {
            var season = document.getElementById('selectSeason').value;
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var teams = JSON.parse(this.responseText);
                    var selectTeam = document.getElementById('selectTeam');
                    selectTeam.innerHTML = "";
                    teams.forEach(function(team) {
                        var option = document.createElement('option');
                        option.value = team;
                        option.textContent = team;
                        selectTeam.appendChild(option);
                    });

                    
                    if (teams.length > 0) {
                        loadPlayers();
                    } else {
                        var squadcardContainer = document.querySelector('.squad-cards-container');
    squadcardContainer.innerHTML = "<img class='table-image' src='media/icons/proximo_icon.png' alt='table_icon' />";
                    }
                }
            };
            xhttp.open("GET", "php/operations/get_teams.php?season=" + season, true);
            xhttp.send();
        }
    




        function loadPlayers() {
            var selectedTeam = document.getElementById('selectTeam').value;
            var season = document.getElementById('selectSeason').value;
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4) {
                    if (this.status == 200) {
                        try {
                            var players = JSON.parse(this.responseText);
                            //
                            console.log('Response Text:', this.responseText);
//
                            var squadcardContainer = document.querySelector('.squad-cards-container');
                            squadcardContainer.innerHTML = "";

                            var sliderWrapper = document.createElement('div');
                            sliderWrapper.classList.add('slider-wrapper');

                            var leftSlidebutton = document.createElement('button');
                            leftSlidebutton.classList.add('flow-left-button');
                            leftSlidebutton.classList.add('flow-button');
                            leftSlidebutton.id = 'prev-silde';

                            var leftIconImage = document.createElement('img');
                            leftIconImage.src = "media/icons/arrow_left_white_icon.png";
                            leftIconImage.alt = "arrow_left_white_icon";

                            var imageList = document.createElement('div');
                            imageList.classList.add('image-list');

                            var rightSlidebutton = document.createElement('button');
                            rightSlidebutton.classList.add('flow-right-button');
                            rightSlidebutton.classList.add('flow-button');
                            rightSlidebutton.id = 'next-silde';

                            var rightIconImage = document.createElement('img');
                            rightIconImage.src = "media/icons/arrow_right_white_icon.png";
                            rightIconImage.alt = "arrow_right_white_icon";

                            players.forEach(function(player) {
                                var nameContainer = document.createElement('span');
                                nameContainer.classList.add('name-container');

                                var image = document.createElement('img');
                                image.classList.add('image-item');
                                image.src = player.imageSrc;
                                image.alt = player.altText;

                                var button = document.createElement('button');
                                button.classList.add('name-button');

                                var playerName = document.createElement('p');
                                playerName.classList.add('player-name');
                                playerName.textContent = player.name;

                                button.appendChild(playerName);
                                nameContainer.appendChild(image);
                                nameContainer.appendChild(button);
                                leftSlidebutton.appendChild(leftIconImage);
                                sliderWrapper.appendChild(leftSlidebutton);
                                imageList.appendChild(nameContainer);

                                sliderWrapper.appendChild(imageList);

                                rightSlidebutton.appendChild(rightIconImage);
                                sliderWrapper.appendChild(rightSlidebutton);

                                squadcardContainer.appendChild(sliderWrapper);
                            });

                        } catch (error) {
                            console.error('Error parsing JSON:', error);
                            console.log('Response Text:', this.responseText);
                        }
                    } else {
                        console.error('Request failed with status:', this.status);
                    }
                }
            };
            xhttp.open("GET", "php/operations/get_players.php?season=" + season + "&team=" + selectedTeam + "&rand=" + Math.random(), true);
            //xhttp.open("GET", "php/operations/get_players.php?season=" + season + "&team=" + selectedTeam, true);
            xhttp.send();
        }
        
    

    </script>


    <script src="scripts/squad-slider.js"></script>


    <?php include('php/templates/footer.php');  ?>


</body>

</html>