<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      rel="icon"
      type="image/x-icon"
      href="media/icons/favicon.ico"
    />
    <title>Pagina no encontrada &#x21aa; worldrugby.org</title>
    <link rel="stylesheet" href="estilos/general.css" />
    <link rel="stylesheet" href="estilos/header.css" />
    <link rel="stylesheet" href="estilos/headerMediaQuery.css" />
    <link rel="stylesheet" href="estilos/footer.css" />
    <link rel="stylesheet" href="estilos/footerMediaQuery.css" />
    <link rel="stylesheet" href="estilos/404.css" />
  </head>
  <body>
  <?php include('php/templates/header.php');  ?>
    <script src="scripts/header.js"></script>
   
    <div class="wrapper">
      <h1 class="title-text">PAGINA NO ENCONTRADA "404"</h1>
      <p class="refrence-text">Parece que la página que buscas ya no existe.</p>
      <a class="links" href="noticias.php"
        >Haga clic aquí para ver noticias</a
      >
      <a class="links" href="index.php">
        Haga clic aquí para volver a la pagina de inicio</a
      >
    </div>
   
    <?php include('php/templates/footer.php');  ?>
  </body>
</html>
