<?php
session_start(); // Start the session

if (isset($_SESSION['logged_in'])) {
  header("Location: 404.php");
  exit;
}
?>

<?php
$error = '';

if (isset($_POST['login'])) {
    $email = $_POST['email'];
    $password = md5($_POST['password']);

    $userFilePath = 'data/users/users.xml';

    if (file_exists($userFilePath)) {
        $xml = @simplexml_load_file($userFilePath);

        if ($xml !== false) {
            $authenticated = false;
            
            foreach ($xml->user as $user) {
            
                if ( $email == $user->email ) {
                  if ($password == $user->password) {
                  echo "Hola3";
                    session_start();
                    $_SESSION['username'] = (string)$user->name;
                    $_SESSION['email'] = (string)$user->email;
                    $_SESSION['role'] = (string)$user->role;
                    $_SESSION['password'] = (string)$user->password;
                    $_SESSION['logged_in'] = true;
                    $authenticated = true;
                    break;
                  }
                }
            }
            if (!$authenticated) {
                $error = "Contraseña o email erróneos.";
            } else {
                $error = '';
            }
        } else {
            $error = "Error parsing user data.";
        }
    } else {
        $error = "User data file not found.";
    }

    if (!$error) {
      header('Location: index.php');
      die;
    } 
    $opacity = ($error !== '') ? "1" : "0";
}
?>



<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      rel="icon"
      type="image/x-icon"
      href="media/logos/favicon.ico"
    />
    <title>Inicio de sesion &#x21aa; worldrugby.org</title>
    <link rel="stylesheet" href="estilos/sign-up/login-form.css" />
    <link rel="stylesheet" href="estilos/general.css" />
    <link rel="stylesheet" href="estilos/header.css" />
    <link rel="stylesheet" href="estilos/headerMediaQuery.css" />
    <link rel="stylesheet" href="estilos/footer.css" />
    <link rel="stylesheet" href="estilos/footerMediaQuery.css" />
    <style>
        #authentication-error {
            opacity: <?php echo $opacity; ?>;
        }
    </style>
  </head>
  <body>

  <?php include('php/templates/header.php');  ?>
    <script src="scripts/header.js"></script>


    <section class="wrapper">
      <div class="form login">
        <header class="title">Inicio de Sesión</header>
        <form class="login-form" method="post" action="">
          <input
            type="text"
            id="email-login"
            placeholder="Correo electrónico"
            required
            name="email"
            onchange="return validateEmail('email-login')"
          />
          <span class="error" id="email-login-error"></span>
          <input
            id="password-login"
            type="password"
            name="password"
            placeholder="Contraseña"
            required
          />
          <span class="error" id="password-login-error"></span>
          <a class="password-reset" href="#">¿Has olvidado tu contraseña?</a>
          <input type="submit" value="Login" name="login" />
          
          <span id="authentication-error"><?php echo $error; ?></span>
        </form>
      </div>
      <script src="scripts/login-validation.js"></script>
    </section>

    <?php include('php/templates/footer.php');  ?>
  </body>
</html>
