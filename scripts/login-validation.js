function validateEmail() {
  var emailInput = document.getElementById("email-login");
  var emailError = document.getElementById("email-login-error");
  emailInput.style.border = "none";

  emailError.textContent = "";

  var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (!emailRegex.test(emailInput.value)) {
    emailError.classList.add("active");
    emailInput.style.border = "solid 1px red";
    emailError.textContent = "El formato del email es invalido";
    return false;
  }
  return true;
}