function validateName() {
    var nameInput = document.getElementById("name");
    var nameError = document.getElementById("nameError");
    nameError.textContent = "";
    nameInput.style.border = "none";

    var nameRegex = /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u;
    if (!nameRegex.test(nameInput.value)) {
        nameError.classList.add("active");
        nameInput.style.border = "solid 1px red";
        nameError.textContent = "El nombre no debe contener números";
        return false;
    }
    return true;
}

function validatePhone() {
    var phoneInput = document.getElementById("phone");
    var phoneError = document.getElementById("phoneError");
    phoneError.textContent = "";
    phoneInput.style.border = "none";

    var phoneNumber = phoneInput.value;
    if (isNaN(phoneNumber) || phoneNumber.length !== 9) {
        phoneError.classList.add("active");
        phoneInput.style.border = "solid 1px red";
        phoneError.textContent = "El número debe contener 9 dígitos";
        return false;
    }
    return true;
}

function validateEmail() {
    var emailInput = document.getElementById("email");
    var emailError = document.getElementById("emailError");
    emailError.textContent = "";
    emailInput.style.border = "none";

    var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(emailInput.value)) {
        emailError.classList.add("active");
        emailInput.style.border = "solid 1px red";
        emailError.textContent = "El formato del email es invalido";
        return false;
    }
    return true;
}

function validateSubject() {
    var subject = document.getElementById("subject");
    var subjectError = document.getElementById("subjectError");
    subjectError.textContent = "";
    if (subject.value === "a") {
        subjectError.classList.add("active");
        subject.style.border = "solid 1px red";
        subjectError.textContent = "Hay que elegir un asunto";
        return false;
    }
    subject.style.border = "none";
    return true;
}

function validateMessage() {
    var messageInput = document.getElementById("message");
    var messageError = document.getElementById("messageError");
    messageError.textContent = "";
    messageInput.style.border = "none";

    if (messageInput.value.trim() === "") {
        messageError.classList.add("active");
        messageInput.style.border = "solid 1px red";
        messageError.textContent = "El mensaje no puede estar vacío";
        return false;
    }
    return true;
}

function showModal(message, isSuccess) {
    var modal = document.getElementById("statusModal");
    var statusMessage = document.getElementById("statusMessage");
    var closeBtn = document.querySelector(".close-btn");

    statusMessage.textContent = message;
    modal.style.display = "block";

    closeBtn.onclick = function() {
        modal.style.display = "none";
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}

contactForm.addEventListener("submit", function(e) {
    e.preventDefault();
    console.log("Form submitted");
    if (validateForm()) {
        const formData = new FormData(contactForm);
        fetch('php/operations/mensaje.php', {
            method: 'POST',
            body: formData,
        })
        .then(response => {
            console.log("Response received", response);
            return response.json();
        })
        .then(data => {
            console.log("Data received", data);
            showModal(data.message, data.status === 'success');
        })
        .catch(error => {
            console.error("Error occurred", error);
            showModal('Произошла ошибка при отправке письма.', false);
        });
    }
});

function validateForm() {
    return validateName() && validatePhone() && validateEmail() && validateSubject() && validateMessage();
}

	
