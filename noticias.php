<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      rel="icon"
      type="image/x-icon"
      href="media/logos/favicon.ico"
    />
    <link rel="stylesheet" href="estilos/general.css" />
    <link rel="stylesheet" href="estilos/header.css" />
    <link rel="stylesheet" href="estilos/headerMediaQuery.css" />
    <link rel="stylesheet" href="estilos/noticias/main.css" />
    <link rel="stylesheet" href="estilos/noticias/mainMediaQuery.css" />
    <link rel="stylesheet" href="estilos/footer.css" />
    <link rel="stylesheet" href="estilos/footerMediaQuery.css" />
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <title>Noticias &#x21aa; worldrugby.org</title>
  </head>

  <body class="NewsArticleListPage">
 

  <?php include('php/templates/header.php');  ?>
  <script src="scripts/header.js"></script>
    <main>
      <div class="top-section-container">
        <div class="text-container">
          <h1 class="title narrow">Noticias</h1>
        </div>
      </div>

      <div class="narrow">
        <div class="article-title-container">
          <h2 class="news-title">Todas las noticias</h2>
        </div>
      </div>

      <section class="narrow">
        <div class="select-container">
          <select id="YearSelector" class="custom-select">
            <option value="all">Todos los años</option>

            <option id="select_option" value="2024">2024</option>

            <option id="select_option" value="2023">2023</option>

          </select>
        </div>

        <ul class="news-articles-list">
          <li>
            <div class="news-item">
              <div class="content">
                <a
                  href="noticias/"
                >
                  <h3>Tuipulotu liderará a los Blues en 2024</h3>
                </a>
                <div class="date">
                  <time datetime="2024-01-16 11:03:29"
                    >16 DE ENERO DE 2024</time
                  >
                </div>

                <div class="image-container-mobile">
                  <img
                    src="media/news/thumbnail/Patrick.jpg"
                    alt="Patrick"
                  />
                </div>

                <p class="summary">
                  El bloqueo de los All Blacks, Patrick Tuipulotu, será el
                  capitán de los Blues para la temporada 2024 de Super Rugby
                  Pacific. Tuipulotu, quien lideró al equipo en 2019-21, dijo
                  que la capitanía era algo que no se tomaba a la ligera.
                </p>
                <a
                  href="noticias/"
                  class="read-more"
                  >Leer más</a
                >
              </div>

              <div class="image-container-desktop">
                <img
                  src="media/news/thumbnail/Patrick.jpg"
                  alt="Patrick"
                />
              </div>
            </div>
          </li>

          <li>
            <div class="news-item">
              <div class="content">
                <a href="noticias/">
                  <div class="img">
                    <h3>
                      El ex segunda linea de Highlander Tom Franklin firma con
                      Western Force
                    </h3>
                  </div>
                </a>
                <div class="date">
                  <time datetime="2024-01-12 11:03:29"
                    >12 DE ENERO DE 2024</time
                  >
                </div>

                <div class="image-container-mobile">
                  <img
                    src="media/news/thumbnail/tom_franklin.jpg"
                    alt="tom_franklin"
                  />
                </div>

                <p class="summary">
                  El ex segunda linea de Highlander y Māori All Blacks, Tom
                  Franklin, regresará a DHL Super Rugby Pacific con Force en
                  2024.
                </p>
                <a href="noticias/" class="read-more"
                  >Leer más</a
                >
              </div>

              <div class="image-container-desktop">
                <div class="img">
                  <img
                    src="media/news/thumbnail/tom_franklin.jpg"
                    alt="tom_franklin"
                  />
                </div>
              </div>
            </div>
          </li>

          <li>
            <div class="news-item">
              <div class="content">
                <a href="noticias/">
                  <h3>
                    Ardie Savea gana el primer premio en los ASB Rugby Awards
                  </h3>
                </a>
                <div class="date">
                  <time datetime="2023-12-14 11:03:29"
                    >14 de DICIEMBRE de 2023</time
                  >
                </div>
                <div class="image-container-mobile">
                  <img
                    src="media/news/thumbnail/Ardie-Savea.jpg"
                    alt="Ardie-Savea-v4"
                  />
                </div>
                <p class="summary">
                  Un mes después de reclamar el premio más importante de World
                  Rugby, Ardie Savea obtuvo el premio al Jugador del Año en
                  Kelvin R Tremain Memorial en los ASB Rugby Awards de esta
                  noche.
                </p>
                <a href="noticias/" class="read-more"
                  >Leer más</a
                >
              </div>
              <div class="image-container-desktop">
                <img
                  src="media/news/thumbnail/Ardie-Savea.jpg"
                  alt="Ardie-Savea-v4"
                />
              </div>
            </div>
          </li>

          <li>
            <div class="news-item">
              <div class="content">
                <a
                  href="noticias/"
                >
                  <h3>Jono Gibbes vuelve a la carga con los Chiefs</h3>
                </a>
                <div class="date">
                  <time datetime="2023-12-14 11:03:29"
                    >14 de DICIEMBRE de 2023</time
                  >
                </div>

                <div class="image-container-mobile">
                  <img
                    src="media/news/thumbnail/jono_gibbes.jpg"
                    alt="jono_gibbes"
                  />
                </div>

                <p class="summary">
                  El peripatético ex All Black Lock Jono Gibbes se unirá a los
                  Chiefs para el Super Rugby Pacific el próximo año como
                  entrenador de recursos y entrenador del Chiefs' Development
                  XV.
                </p>
                <a
                  href="noticias/"
                  class="read-more"
                  >Leer más</a
                >
              </div>

              <div class="image-container-desktop">
                <div class="img">
                  <img
                    src="media/news/thumbnail/jono_gibbes.jpg"
                    alt="jono_gibbes"
                  />
                </div>
              </div>
            </div>
          </li>

          <li>
            <div class="news-item">
              <div class="content">
                <a
                  href="noticias/"
                >
                  <h3>
                    Recuperación, rivalidad y redención: la historia del regreso
                    de TJ Perenara
                  </h3>
                </a>
                <div class="date">
                  <time datetime="2023-12-13 11:03:29"
                    >13 DE DICIEMBRE DE 2023</time
                  >
                </div>

                <div class="image-container-mobile">
                  <img
                    src="media/news/thumbnail/Perenara.jpg"
                    alt="Perenara"
                  />
                </div>

                <p class="summary">
                  Recuperándose de la lesión en el tendón de Aquiles que sufrió
                  en el empate 25-25 con Inglaterra a finales de 2022, el
                  corredor TJ Perenara ha señalado que recuperar su camiseta de
                  los All Blacks es su objetivo a largo plazo.
                </p>
                <a
                  href="noticias/"
                  class="read-more"
                  >Leer más</a
                >
              </div>

              <div class="image-container-desktop">
                <div class="img">
                  <img
                    src="media/news/thumbnail/Perenara.jpg"
                    alt="Perenara"
                  />
                </div>
              </div>
            </div>
          </li>

          <li>
            <div class="news-item">
              <div class="content">
                <a
                  href="noticias/"
                >
                  <h3>
                    Los planes de Lakai regresan al campo para los huracanes
                  </h3>
                </a>
                <div class="date">
                  <time datetime="2023-12-12 11:03:29"
                    >12 DE DICIEMBRE DE 2023
                  </time>
                </div>

                <div class="image-container-mobile">
                  <img
                    src="media/news/thumbnail/Peter-Lakai.jpg"
                    alt="Peter-Lakai"
                  />
                </div>

                <p class="summary">
                  El delantero suelto de los Hurricanes emergentes, Peter Lakai,
                  pretende empezar a trabajar cuando se reanude el entrenamiento
                  de pretemporada en enero.
                </p>
                <a
                  href="noticias/"
                  class="read-more"
                  >Leer más</a
                >
              </div>

              <div class="image-container-desktop">
                <div class="img">
                  <img
                    src="media/news/thumbnail/Peter-Lakai.jpg"
                    alt="h_00067556-1"
                  />
                </div>
              </div>
            </div>
          </li>

          <li>
            <div class="news-item">
              <div class="content">
                <a
                  href="noticias/"
                >
                  <h3>Ratima ve una oportunidad en la temporada 2024</h3>
                </a>
                <div class="date">
                  <time datetime="2023-12-12 11:03:29"
                    >12 DE DICIEMBRE DE 2023</time
                  >
                </div>

                <div class="image-container-mobile">
                  <img
                    src="media/news/thumbnail/Cortez.jpg"
                    alt="Cortez"
                  />
                </div>

                <p class="summary">
                  El corredor de los Rising Chiefs, Cortez Ratima, tendrá muchos
                  incentivos en la preparación del juego DHL Super Rugby Pacific
                  en 2024.
                </p>
                <a
                  href="noticias/"
                  class="read-more"
                  >Leer más</a
                >
              </div>

              <div class="image-container-desktop">
                <div class="img">
                  <img
                    src="media/news/thumbnail/Cortez.jpg"
                    alt="Cortez"
                  />
                </div>
              </div>
            </div>
          </li>

          <li>
            <div class="news-item">
              <div class="content">
                <a
                  href="noticias/"
                >
                  <h3>
                    Millar quiere recuperar el tiempo perdido en los Highlanders
                  </h3>
                </a>
                <div class="date">
                  <time datetime="2023-12-12 11:03:29"
                    >12 DE DICIEMBRE DE 2023
                  </time>
                </div>

                <div class="image-container-mobile">
                  <img
                    src="media/news/thumbnail/Cameron-Millar.jpg"
                    alt="Cameron-Millar"
                  />
                </div>

                <p class="summary">
                  Highlanders y Otago primeros cinco octavos Cameron Millar ha
                  recibido algunas lecciones tempranas sobre cómo ser paciente
                  en su carrera de rugby, pero de cara al DHL Super Rugby
                  Pacific en 2024, está ansioso por recuperar el tiempo perdido.
                </p>
                <a
                  href="noticias/"
                  class="read-more"
                  >Leer más</a
                >
              </div>

              <div class="image-container-desktop">
                <div class="img">
                  <img
                    src="media/news/thumbnail/Cameron-Millar.jpg"
                    alt="Cameron-Millar"
                  />
                </div>
              </div>
            </div>
          </li>

          <li>
            <div class="news-item">
              <div class="content">
                <a href="noticias/">
                  <h3>
                    Sam Darry construye sueños de campeonato con los Blues
                  </h3>
                </a>
                <div class="date">
                  <time datetime="2023-12-11 11:03:29"
                    >11 de diciembre de 2023</time
                  >
                </div>

                <div class="image-container-mobile">
                  <img
                    src="media/news/thumbnail/Sam-Darry.jpg"
                    alt="Sam-Darry"
                  />
                </div>

                <p class="summary">
                  El segunda linea de los Blues, Sam Darry, busca aprovechar su
                  ya impresionante comienzo en el Super Rugby Pacific y ser
                  parte de una unidad con sede en Auckland que juegue
                  consistentemente a su capacidad en 2024.
                </p>
                <a href="noticias/" class="read-more"
                  >Leer más</a
                >
              </div>

              <div class="image-container-desktop">
                <div class="img">
                  <img
                    src="media/news/thumbnail/Sam-Darry.jpg"
                    alt="Sam-Darry"
                  />
                </div>
              </div>
            </div>
          </li>
        </ul>

        <div class="button-container">
          <button
            class="cargarMasBtnClass"
            id="cargarMasBtn"
            onclick="cargarMasNoticias()"
          >
            Cargar más
          </button>
          <button
            class="volverArribaBtnClass"
            id="volverArribaBtn"
            onclick="volverArriba()"
          >
            Volver arriba
          </button>
        </div>
      </section>
    </main>

    <?php include('php/templates/footer.php');  ?>
   
    <script src="scripts/Year-selector.js"></script>
  </body>
</html>
