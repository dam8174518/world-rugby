<?php

if (!isset($_SESSION)) session_start();

if (!isset($_SESSION) || $_SESSION['logged_in']  !== true) {
  header("Location: 404.php"); 
    exit;
}
?>

<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      rel="icon"
      type="image/x-icon"
      href="media/logos/favicon.ico"
    />
    <title>Inicio de sesion &#x21aa; worldrugby.org</title>
    <link rel="stylesheet" href="estilos/sign-up/login-form.css" />
    <link rel="stylesheet" href="estilos/general.css" />
    <link rel="stylesheet" href="estilos/header.css" />
    <link rel="stylesheet" href="estilos/headerMediaQuery.css" />
    <link rel="stylesheet" href="estilos/footer.css" />
    <link rel="stylesheet" href="estilos/footerMediaQuery.css" />
    <link rel="stylesheet" href="estilos/user-page/user-page.css" />
    <link rel="stylesheet" href="estilos/user-page/management-table.css" />
    <style>
        #authentication-error {
            opacity: <?php echo $opacity; ?>;
        }
    </style>
  </head>
  <body>

  <?php include('php/templates/header.php');  ?>
    <script src="scripts/header.js"></script>

    


    <div class="profile-container">
      <div class="profile-card">
        <img src="media/icons/administration-icon.png" alt="image1" class="profile-icon" />
        <div class="profile-name"><?php
    echo ' <p"> ' . $_SESSION['username']  . '</p>   ';
    ?></div>
        <button class="log-out-button">  <?php
    echo '<a class="logout-link"  href="php/operations/logout.php">Cerrar sesion</a>';
    ?></button>
      
      </div>
    </div>


    <?php
   
   if ($_SESSION['role'] == 'admin') {
    echo '<h1 class="table-title">Tabla de usuarios</h1>';
    echo '<table>
            <tr>
                <th>Username</th>
                <th>Email</th>
                <th>Password</th>
                <th>Role</th>
            </tr>';

    $xml = simplexml_load_file('data/users/users.xml');

    foreach ($xml->user as $user) {
        echo '<tr>';
        echo '<td>' . $user->name . '</td>';
        echo '<td>' . $user->email . '</td>';
        echo '<td>' . $user->password . '</td>';
        echo '<td>' . $user->role . '</td>';
        echo '</tr>';
    }

    echo '</table>';
}
?>


    
    
    <?php include('php/templates/footer.php');  ?>
  </body>
</html>