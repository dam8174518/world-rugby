<?php
if (!isset($_SESSION)) session_start();

if (!isset($_SESSION['logged_in']) || $_SESSION['logged_in'] !== true) {
    header("Location: login-form.php");
    exit;
}
?>



<!DOCTYPE html>
<html lang="en">
<head>
<head> 
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      rel="icon"
      type="image/x-icon"
      href="../../media/logos/favicon.ico"
    />
    <link rel="stylesheet" href="estilos/general.css" />
    <link rel="stylesheet" href="estilos/header.css" />
    <link rel="stylesheet" href="estilos/headerMediaQuery.css" />
    <link rel="stylesheet" href="estilos/footer.css" />
    <link rel="stylesheet" href="estilos/footerMediaQuery.css" />
    <link rel="stylesheet" href="estilos/resultados/resultados.css" />
    <script
    src="https://kit.fontawesome.com/6cb64a97a2.js"
    crossorigin="anonymous"
  ></script>
    <title>Resultados &#x21aa; worldrugby.org</title>
  </head>
<body>
<?php include('php/templates/header.php');  ?>
    <script src="scripts/header.js"></script>
   

   


    <div class="select-outer-container">
    <div class="select-container">
    <select class="select-box" name="selectBox" id="xml_file">
       
    </select>
    </div>
    <div class="select-box-icon-container">
          <i class="fa-solid fa-caret-down"></i>
        </div>
    </div>

    <div id="result"></div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            loadXMLFiles();
        });

        document.getElementById('xml_file').addEventListener('change', function() {
            var selectedFile = this.value;
            loadXML(selectedFile);
        });

        
        function loadXMLFiles() {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var files = JSON.parse(this.responseText);
                    var selectBox = document.getElementById('xml_file');
                    files.forEach(function(file) {
                        var fileName = file.split('/').pop();
                        fileName = fileName.split('.').slice(0, -1).join('.'); 
                        var option = document.createElement('option');
                        option.value = file;
                        option.textContent = "Temporada " + fileName;
                        selectBox.appendChild(option);
                    });

                selectBox.selectedIndex = 0;

                handleSelectionChange();
                }
            };
            xhttp.open("GET", "php/operations/get_xml_files.php", true);
            xhttp.send();
        }

        

        function handleSelectionChange() {
        var selectedFile = document.getElementById('xml_file').value;
        loadXML(selectedFile);
    }


    
        function loadXML(file) {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("result").innerHTML = this.responseText;
                }
            };
            xhttp.open("GET", "php/operations/transform.php?file=" + file, true);
            xhttp.send();
        }

        



        

    </script>
      <?php include('php/templates/footer.php');  ?>

</body>
</html>
