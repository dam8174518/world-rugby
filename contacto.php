<?php
if (!isset($_SESSION)) session_start();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" type="image/x-icon" href="media/Imagenes/logos/favicon.ico" />
    <link rel="stylesheet" href="estilos/general.css" />
    <link rel="stylesheet" href="estilos/header.css" />
    <link rel="stylesheet" href="estilos/headerMediaQuery.css" />
    <link rel="stylesheet" href="estilos/footer.css" />
    <link rel="stylesheet" href="estilos/footerMediaQuery.css" />
    <link rel="stylesheet" href="estilos/contacto.css" />
    <script src="https://kit.fontawesome.com/6cb64a97a2.js" crossorigin="anonymous"></script>
    <title>Contacto &#x21aa; worldrugby.org</title>
</head>

<body class="NewsArticleListPage">
<?php include('php/templates/header.php');  ?>
    <script src="scripts/header.js"></script>

    <div class="top-section-container">
        <div class="text-container">
            <h1 class="title">Contacto</h1>
        </div>
    </div>
    <main class="main-container">
        <div class="directions">
            <div class="direction-container">
                <h3 class="direction-title">World Rugby Office details:</h3>
                <p class="direction-text">Rugby House</p>
                <p class="direction-text">Level 3, 100 Molesworth Street</p>
                <p class="direction-text">Wellington 6011</p>
                <p class="direction-text">New Zealand</p>
            </div>
        </div>

        <div class="form-container">
            <form id="contactForm" class="contact-form">
                <div class="info">
                    <div class="input-group">
                        <input type="text" id="name" name="name" required />
                        <label for="name"><i class="fa-solid fa-user icon"></i>Nombre</label>
                        <span class="error" id="nameError"></span>
                    </div>
                    <div class="input-group">
                        <input type="text" id="phone" name="phone" required />
                        <label for="phone"><i class="fas fa-phone-square-alt icon"></i> Nº de telefono</label>
                        <span class="error" id="phoneError"></span>
                    </div>
                </div>
                <div class="input-group">
                    <input type="text" id="email" name="email" required />
                    <label for="email"><i class="fas fa-envelope icon"></i>Email</label>
                    <span class="error" id="emailError"></span>
                </div>
                <div class="input-group">
                    <select id="subject" name="subject" required>
                        <option value="a">Elige un asunto</option>
                        <option value="b">Caridad y donación</option>
                        <option value="c">Opiniones de los fans</option>
                        <option value="d">Entradas</option>
                        <option value="g">Otros</option>
                    </select>
                    <span class="error" id="subjectError"></span>
                </div>
                <div class="input-group">
                    <textarea id="message" name="message" rows="8" required></textarea>
                    <label for="message"><i class="fas fa-comments icon"></i>Tu mensaje</label>
                    <span class="error" id="messageError"></span>
                </div>
                <button class="submit-button" type="submit">
                    Submit <i class="fas fa-paper-plane icon"></i>
                </button>
            </form>
        </div>
        <div id="statusModal" class="modal">
            <div class="modal-content">
                <span class="close-btn">&times;</span>
                <p id="statusMessage"></p>
            </div>
        </div>
    </main>

    <?php include('php/templates/footer.php');  ?>
    <script src="scripts/contactFormHandling.js"></script>
</body>

</html>