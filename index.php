
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      rel="icon"
      type="image/x-icon"
      href="media/logos/favicon.ico"
    />
    <link rel="stylesheet" href="estilos/general.css" />
    <link rel="stylesheet" href="estilos/header.css" />
    <link rel="stylesheet" href="estilos/headerMediaQuery.css" />
    <link rel="stylesheet" href="estilos/footer.css" />
    <link rel="stylesheet" href="estilos/footerMediaQuery.css" />
    <link rel="stylesheet" href="estilos/index/main.css" />
    <link rel="stylesheet" href="estilos/index/mainMediaQuery.css" />
    <link rel="stylesheet" href="estilos/index/videos.css" />
    <link rel="stylesheet" href="estilos/index/competetions.css" />
    <link rel="stylesheet" href="estilos/index/traditions.css" />
    <link rel="stylesheet" href="estilos/index/newsSlider.css" />
    <title>Inicio &#x21aa; worldrugby.org</title>
  </head>
  <body>
    
    <?php include('php/templates/header.php');  ?>
    <script src="scripts/header.js"></script>
    <!--Add comentes later !!!!-->
    <main>
      <div class="news-container">
        <a
          class="primary-article-container"
          href="noticias/cuando-springboks-supo-que-ganaria-la-liga.php"
        >
          <div class="primary-article-container-info">
            <p class="main-article-title">
              EL MOMENTO EN QUE SPRINGBOKS SUPO QUE GANARÍA LA LIGA
            </p>
            <p class="main-article-text">
              Pieter-Steph du Toit dijo que un incidente de ocho minutos en el
              juego...
            </p>
            <p class="publiched-at">Hace un día</p>
            <button class="read-more-button">Leer Más</button>
          </div>
        </a>
        <div class="secondary-articles-container">
          <div class="important-news-outer-container">
            <div class="important-news-headers">
              <h4 class="last-news-header">ULTIMAS NOTICIAS</h4>
              <div class="see-more-section">
                <a href="noticias.php"> Ver todo</a>
                <img
                  class="arrow-right-icon"
                  src="media/icons/arrow_right_icon.png"
                  alt="arrow_right_icon"
                />
              </div>
            </div>
            <div class="important-news-inner-container">
              <a
                href="noticias/all-blacks-dessarollan-la-pretempurada-en-cardiff.php"
                class="second-article-one"
              >
                <div class="second-article-info">
                  <p class="secondary-article-title">
                    All Blacks continúan el desarrollo de la pretemporada en
                    Cardiff
                  </p>
                  <p class="secondary-article-text">
                    Jordan mostró en la victoria 54-16 de los All Blacks sobre
                    Shamrock...
                  </p>
                  <p class="publiched-at-secondary">HACE 2 DIAS</p>
                </div>
              </a>

              <a
                href="noticias/sam-cane-ahora-es-capitan-del-red-rose.php"
                class="second-article-two"
              >
                <div class="second-article-info">
                  <p class="secondary-article-title">
                    Samuel Cane es el nuevo capitán de Red Rose
                  </p>
                  <p class="secondary-article-text">
                    Red Rose se sueltan hacia adelante y Sam Cane ha sido
                    nombrado como...
                  </p>
                  <p class="publiched-at-secondary">HACE 6 DIAS</p>
                </div>
              </a>
            </div>
            <div class="unimportant-news-container">
              <a
                href="noticias/miller-confirma-jugar-para-los ferns.php"
                class="third-article-one"
              >
                <div class="third-article-info">
                  <p class="third-article-title">
                    Miller confirma jugar para los Ferns
                  </p>
                  <p class="third-article-text">
                    El contrato de cuatro años es el más largo para...
                  </p>
                  <p class="publiched-at-third">HACE 10 DIAS</p>
                </div>
              </a>
              <a
                href="noticias/sakuras-frente-a-shamrock-en-cuartos.php"
                class="third-article-two"
              >
                <div class="third-article-info">
                  <p class="third-article-title">
                    Sakuras frente a Shamrock en cuartos
                  </p>
                  <p class="third-article-text">
                    Rugby unión se complace en anunciar que...
                  </p>
                  <p class="publiched-at-third">HACE 13 DIAS</p>
                </div>
              </a>
              <a
                href="noticias/los-wallabies-tienen-mucho-para-construir.php"
                class="third-article-three"
              >
                <div class="third-article-info">
                  <p class="third-article-title">
                    Los wallabies tienen mucho para construir
                  </p>
                  <p class="third-article-text">
                    Hooker, quien anotó el último de los ensayos de...
                  </p>
                  <p class="publiched-at-third">HACE 14 DIAS</p>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>

      <div class="news-slider-container">
        <div class="news-slider-header">
          <p class="news-header-title">ULTIMAS NOTICIAS</p>
          <div class="news-see-more-section">
            <a href="noticias.html">Ver todo</a>
            <img
              class="news-arrow-right-icon"
              src="media/icons/arrow_right_icon.png"
              alt="arrow_right_icon"
            />
          </div>
        </div>
        <div class="news-cards-container">
          <div class="news-slider-wrapper">
            <button
              id="news-prev-silde"
              class="news-flow-left-button news-flow-button"
            >
              <img
                src="media/icons/arrow_left_white_icon.png"
                alt="arrow_left_white_icon"
              />
            </button>
            <div class="news-image-list">
              <span class="article-container">
                <img
                  id="first-child-image"
                  class="news-image-item"
                  src="media/news/thumbnail/all_blacks_pretemporada.jpg"
                  alt="cape_town_preview"
                />
                <button class="article-button article-button-first-child">
                  <p class="article-title">
                    All Blacks continúan el desarrollo de la pretemporada en
                    Cardiff
                  </p>
                  <p class="article-text">
                    Jordan mostró en la victoria 54-16 de los All Blacks sobre
                    Shamrock...
                  </p>
                  <p class="publiched-at">HACE 2 DIAS</p>
                </button>
              </span>
              <span class="article-container">
                <img
                  class="news-image-item"
                  src="media/news/thumbnail/same_cane_red_rose_new_capitan.jpg"
                  alt="cape_town_preview"
                />
                <button class="article-button">
                  <p class="article-title">
                    Samuel Cane es el nuevo capitán de Red Rose
                  </p>
                  <p class="article-text">
                    Red Rose se sueltan hacia adelante y Sam Cane ha sido
                    nombrado como...
                  </p>
                  <p class="publiched-at">HACE 6 DIAS</p>
                </button>
              </span>
              <span class="article-container">
                <img
                  class="news-image-item"
                  src="media/news/thumbnail/miller_to_black_fernes.jpg"
                  alt="cape_town_preview"
                />
                <button class="article-button">
                  <p class="article-title">
                    Miller confirma jugar para los Ferns
                  </p>
                  <p class="article-text">
                    El contrato de cuatro años es el más largo para...
                  </p>
                  <p class="publiched-at">HACE 10 DIAS</p>
                </button>
              </span>

              <span class="article-container">
                <img
                  class="news-image-item"
                  src="media/news/thumbnail/sakuras_stadium.jpg"
                  alt="cape_town_preview"
                />
                <button class="article-button">
                  <p class="article-title">
                    Sakuras frente a Shamrock en cuartos
                  </p>
                  <p class="article-text">
                    Rugby unión se complace en anunciar que...
                  </p>
                  <p class="publiched-at">HACE 13 DIAS</p>
                </button>
              </span>

              <span class="article-container">
                <img
                  class="news-image-item"
                  src="media/news/thumbnail/wallabies_new_coach.jpg"
                  alt="cape_town_preview"
                />
                <button class="article-button">
                  <p class="article-title">
                    Los wallabies tienen mucho para construir
                  </p>
                  <p class="article-text">
                    Hooker, quien anotó el último de los ensayos de...
                  </p>
                  <p class="publiched-at">HACE 14 DIAS</p>
                </button>
              </span>
            </div>
            <button
              id="news-next-silde"
              class="news-flow-right-button news-flow-button"
            >
              <img
                src="media/icons/arrow_right_white_icon.png"
                alt="arrow_right_white_icon"
              />
            </button>
          </div>
        </div>
      </div>

      <script src="scripts/news-slider.js"></script>
      <!--Add comentes later !!!!-->

      <div class="latest-videos-container">
        <div class="latest-videos-header">
          <p class="latest-videos-header-title">ULTIMOS VIDEOS</p>
          <div class="see-more-section">
            <a href="videos.php">Ver todo</a>
            <img
              class="arrow-right-icon"
              src="media/icons/arrow_right_icon.png"
              alt="arrow_right_icon"
            />
          </div>
        </div>
        <div class="video-cards-container">
          <div class="slider-wrapper">
            <button id="prev-silde" class="flow-left-button flow-button">
              <img
                src="media/icons/arrow_left_white_icon.png"
                alt="arrow_left_white_icon"
              />
            </button>
            <div class="image-list">
              <span class="play-container">
                <img
                  id="first-child-image"
                  class="image-item"
                  onclick="location.href='noticias/preview-cape-town-svns-2023.php'"
                  src="media/news/thumbnail/cape_town_preview.jpg"
                  alt="cape_town_preview"
                />
                <a
                  class="video-link"
                  href="noticias/preview-cape-town-svns-2023.php"
                  ><button class="play-button">
                    <img
                      class="play-icon"
                      src="media/icons/play_icon.png"
                      alt="play_icon"
                    /></button
                ></a>
              </span>

              <span class="play-container">
                <img
                  onclick="location.href='noticias/fans-go-crazy-in-lyon.php'"
                  src="media/news/thumbnail/fans_in_lyon.jpg"
                  alt="/fans_in_lyon"
                  class="image-item"
                />
                <a
                  class="video-link"
                  href="noticias/preview-cape-town-svns-2023.php"
                  ><button class="play-button">
                    <img
                      class="play-icon"
                      src="media/icons/play_icon.png"
                      alt="play_icon"
                    /></button
                ></a>
              </span>

              <span class="play-container">
                <img
                  src="media/news/thumbnail/fans_try.jpg"
                  alt="fans_try"
                  class="image-item"
                />
                <a
                  class="video-link"
                  href="videos/preview-cape-town-svns-2023.html"
                  ><button class="play-button">
                    <img
                      class="play-icon"
                      src="media/icons/play_icon.png"
                      alt="play_icon"
                    /></button
                ></a>
              </span>
              <span class="play-container">
                <img
                  src="media/news/thumbnail/rugby_award.jpg"
                  alt="rugby_award"
                  class="image-item"
                />
                <a
                  class="video-link"
                  href="noticias/preview-cape-town-svns-2023.html"
                  ><button class="play-button">
                    <img
                      class="play-icon"
                      src="media/icons/play_icon.png"
                      alt="play_icon"
                    /></button
                ></a>
              </span>
              <span class="play-container">
                <img
                  src="media/news/thumbnail/same_cane.jpg"
                  alt="same_cane"
                  class="image-item"
                />
                <a
                  class="video-link"
                  href="videos/preview-cape-town-svns-2023.html"
                  ><button class="play-button">
                    <img
                      class="play-icon"
                      src="media/icons/play_icon.png"
                      alt="play_icon"
                    /></button
                ></a>
              </span>
              <span class="play-container">
                <img
                  src="media/news/thumbnail/rugby_recoded.jpg"
                  alt="rugby_recoded"
                  class="image-item"
                />
                <a
                  class="video-link"
                  href="videos/preview-cape-town-svns-2023.html"
                  ><button class="play-button">
                    <img
                      class="play-icon"
                      src="media/icons/play_icon.png"
                      alt="play_icon"
                    /></button
                ></a>
              </span>
              <span class="play-container">
                <img
                  src="media/news/thumbnail/press_conference.jpg"
                  alt="press_conference"
                  class="image-item"
                />
                <a
                  class="video-link"
                  href="videos/preview-cape-town-svns-2023.html"
                  ><button class="play-button">
                    <img
                      class="play-icon"
                      src="media/icons/play_icon.png"
                      alt="play_icon"
                    /></button
                ></a>
              </span>
              <span class="play-container">
                <img
                  src="media/news/thumbnail/match_highlights.jpg"
                  alt="match_highlights"
                  class="image-item"
                />
                <a
                  class="video-link"
                  href="videos/preview-cape-town-svns-2023.html"
                  ><button class="play-button">
                    <img
                      class="play-icon"
                      src="media/icons/play_icon.png"
                      alt="play_icon"
                    /></button
                ></a>
              </span>
            </div>

            <button id="next-silde" class="flow-right-button flow-button">
              <img
                src="media/icons/arrow_right_white_icon.png"
                alt="arrow_right_white_icon"
              />
            </button>
          </div>
        </div>
      </div>

      <script src="scripts/videos-slider.js"></script>

      <!--Add comentes later !!!!-->
      <div class="competetion-container">
        <p class="competetion-title">COMPETECIONES</p>
        <div class="image-list-competitions">
          <img
            src="media/logos/pacific.png"
            alt="pacific-league"
            class="competetion"
          />
          <img
            src="media/logos/bunnings-warehouse-fpc.png"
            alt="bunnings-warehouse-fpc"
            class="competetion"
          />
          <img
            src="media/logos/bunnings-warehouse-heartland-championship.png"
            alt="bunnings-warehouse-heartland-championship"
            class="competetion"
          />
          <img
            src="media/logos/super_rugby.png"
            alt="super_rugby"
            class="competetion"
          />
          <img
            src="media/logos/bunnings-warehouse.png"
            alt="bunnings-warehouse"
            class="competetion"
          />
        </div>
      </div>

      <!--Add comentes later !!!!-->
      <div class="tradition-container">
        <div class="tradition-text-container">
          <div class="tradition-text-sub-container">
            <h1 class="tradition-title">LA HAKA</h1>
            <h3 class="tradition-sub-title">Orígenes e Historia de la Haka</h3>
            <p class="tradition-text">
              Para la mayoría de los no maoríes hoy en día su conocimiento de la
              Haka se limita quizás a la más interpretada de Haka llamada "Ka
              mate, Ka mate", que fue compuesta por Ngati Toa Chieftain Te
              Rauparaha alrededor de 1820. Muchos equipos deportivos y personas
              que viajan desde Nueva Zelanda al extranjero tienden a tener el
              haka "Kamate" como parte de su programa.
            </p>
            <a
              class="haka-link"
              href="https://es.wikipedia.org/wiki/Haka"
              target="_blank"
              ><button class="tradition-buttons">Descubre más</button></a
            >
          </div>
        </div>
        <div class="tradition-image-container">
          <img
            class="tradition-image"
            src="media/archive/haka_history.jpg"
            alt="haka_history"
          />
        </div>
      </div>
    </main>





    <?php include('php/templates/footer.php');  ?>


    <?php  include('php/operations/xml-parser.php');
          include('php/operations/parse_clasificacion.php');
    ?>



  </body>
</html>
