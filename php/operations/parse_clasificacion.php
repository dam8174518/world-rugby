<?php



$dir = 'data/seasons';

// Get all XML files in the directory
$files = glob("$dir/*.xml");


// Load XML from file
//$xmlFile = 'data/seasons/*.xml';
$year = 2024;


function compareTeamData($a, $b) {
    // Calculate differences
    $winsDiff = $b['victorias'] - $a['victorias'];
    if ($winsDiff != 0) {
        return $winsDiff;
    }
    $tiesDiff = $b['empates'] - $a['empates'];
    if ($tiesDiff != 0) {
        return $tiesDiff;
    }
    $lossesDiff = $a['derrotas'] - $b['derrotas'];
    if ($lossesDiff != 0) {
        return $lossesDiff;
    }
    return $b['puntos'] - $a['puntos'];
}

foreach ($files as $xmlFile) {

$xmlString = file_get_contents($xmlFile);

// Load the XML
$xml = simplexml_load_string($xmlString);

// Array to store team data
$teamData = [];

foreach ($xml->temporada->resultados->jornada as $jornada) {
    foreach ($jornada->partido as $partido) {
        // Get local and visitor team names
        $localTeam = (string)$partido->equipo_local->image;
        $visitorTeam = (string)$partido->equipo_visitante->image;

        // Update local team data
        if (!isset($teamData[$localTeam])) {
            $teamData[$localTeam] = [
                'partidos' => 0,
                'victorias' => 0,
                'derrotas' => 0,
                'empates' => 0,
                'puntos' => 0,
            ];
        }
        

        // Update visitor team data
        if (!isset($teamData[$visitorTeam])) {
            $teamData[$visitorTeam] = [
                'partidos' => 0,
                'victorias' => 0,
                'derrotas' => 0,
                'empates' => 0,
                'puntos' => 0,
            ];
        }
       
  
        if ($partido->equipo_local->puntos == ' ? ' || $partido->equipo_visitante->puntos == ' ? ') {

            continue;
        }

        // Check if the game is played
      
            // The game is played
           
            $localPoints = (int)$partido->equipo_local->puntos;
            $visitorPoints = (int)$partido->equipo_visitante->puntos;

            if ($localPoints > $visitorPoints) {
                // Local team wins
                $teamData[$localTeam]['victorias']++;
                $teamData[$localTeam]['puntos'] += $localPoints;
                $teamData[$localTeam]['partidos']++;
                $teamData[$visitorTeam]['puntos'] +=  $visitorPoints;
                $teamData[$visitorTeam]['derrotas']++;
                $teamData[$visitorTeam]['partidos']++;
            } elseif ($localPoints < $visitorPoints) {
                // Visitor team wins
                $teamData[$visitorTeam]['victorias']++;
                $teamData[$visitorTeam]['puntos'] +=  $visitorPoints;
                $teamData[$localTeam]['partidos']++;
                $teamData[$localTeam]['puntos'] += $localPoints;
                $teamData[$localTeam]['derrotas']++;
                $teamData[$visitorTeam]['partidos']++;
            } else {
                // Draw
                $teamData[$localTeam]['empates']++;
                $teamData[$visitorTeam]['empates']++;
                $teamData[$localTeam]['puntos'] += $localPoints;
                $teamData[$localTeam]['partidos']++;
                $teamData[$visitorTeam]['puntos'] +=  $visitorPoints;
                $teamData[$visitorTeam]['partidos']++;
            }
        
    }
}


// Sort team data array using custom comparison function
uasort($teamData, 'compareTeamData');


// Create a new SimpleXMLElement for the desired structure
$newXml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?>
<clasificacion xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="clasificacion.xsd"></clasificacion>');
$posicion = 0;

// Loop through each team data and add it to the new XML
foreach ($teamData as $teamName => $data) {
    // Increment position for each team
    $posicion++;

    $fila = $newXml->addChild('fila');
    $fila->addAttribute('id', $teamName);

    $fila->addChild('posicion', $posicion);
    $equipoElement = $fila->addChild('equipo');
    $equipoElement->addChild('logo', $teamName);

    // Get team name from XML
    $teamName = '';
    $equipoNodes = $xml->xpath("//equipo[@name='$teamName']");
    if (!empty($equipoNodes)) {
        $teamName = ucwords((string)$equipoNodes[0]['name']);
    }
    $equipoElement->addChild('nombre', $teamName);

    $fila->addChild('partidos', $data['partidos']);
    $fila->addChild('victorias', $data['victorias']);
    $fila->addChild('derrotas', $data['derrotas']);
    $fila->addChild('empates', $data['empates']);

    // Check if the game is played
    if (!empty($data['puntos'])) {
        $fila->addChild('puntos', $data['puntos']);
    } else {
        // Game not played
        $fila->addChild('puntos', '0');
    }
}

// Format the new XML
$dom = dom_import_simplexml($newXml)->ownerDocument;
$dom->formatOutput = true;

// Print or save the new XML
$newXmlFile = 'data/clasificacion/'. $year . '.xml';
file_put_contents($newXmlFile, $dom->saveXML());
$year++;
}
?>
