<?php
$xml = simplexml_load_file("data/temporadas.xml") or die("No se puede cargar el archivo");

$output_parent_folder = 'data/';
$resultados_folder = $output_parent_folder . 'resultados/';

foreach ($xml->temporada as $temporada) {
    // Create a folder for the current temporada
    $temporada_folder = $output_parent_folder . 'equipos_' . $temporada['id'] . '/';
    if (!file_exists($temporada_folder)) {
        mkdir($temporada_folder, 0777, true);
    }


    $season_folder = $output_parent_folder . 'seasons/';
    if (!file_exists($season_folder) ) {
        mkdir($season_folder, 0777, true);
    }

    // Create a new XML file for temporada
    $temporada_xml = new DOMDocument('1.0', 'UTF-8');
    $temporadas_element = $temporada_xml->createElement('temporadas');
    $temporada_xml->appendChild($temporadas_element);
    $dom_temporada = dom_import_simplexml($temporada);
    $dom_temporada = $temporada_xml->importNode($dom_temporada, true);
    $temporadas_element->appendChild($dom_temporada);

    // Add XML stylesheet declaration
    $stylesheet_instruction = $temporada_xml->createProcessingInstruction('xml-stylesheet', 'href="resultados.xsl" type="text/xsl"');
    $temporada_xml->insertBefore($stylesheet_instruction, $temporadas_element);

    $filename_temporada = $season_folder . $temporada['id'] . '.xml';
    $temporada_xml->save($filename_temporada);

    // Create a new XML file for resultados
    $resultados_xml = new DOMDocument('1.0', 'UTF-8');
    $resultados_element = $resultados_xml->createElement('resultados');
    $resultados_xml->appendChild($resultados_element);

    foreach ($temporada->resultados->jornada as $jornada) {
        // Import each <jornada> element and its children into the new DOMDocument
        $dom_jornada = dom_import_simplexml($jornada);
        $dom_jornada = $resultados_xml->importNode($dom_jornada, true);
        $resultados_element->appendChild($dom_jornada);
    }

    // Add XML stylesheet declaration
    $stylesheet_instruction = $resultados_xml->createProcessingInstruction('xml-stylesheet', 'href="resultados.xsl" type="text/xsl"');
    $resultados_xml->insertBefore($stylesheet_instruction, $resultados_element);

    $filename_resultados = $resultados_folder  . $temporada['id'] . '.xml';
    $resultados_xml->save($filename_resultados);


    // Iterate over each <equipo> element in the current temporada
    foreach ($temporada->equipos->equipo as $equipo) {
        // Create a new DOMDocument
        $equipo_xml = new DOMDocument('1.0', 'UTF-8');
        
        // Create a new <equipos> element
        $equipos_element = $equipo_xml->createElement('equipos');
        $equipo_xml->appendChild($equipos_element);

        // Import the current <equipo> element and its children into the new DOMDocument
        $dom_equipo = dom_import_simplexml($equipo);
        $dom_equipo = $equipo_xml->importNode($dom_equipo, true);
        $equipos_element->appendChild($dom_equipo);
        
        // Define the filename based on the equipo name attribute
        $filename_equipo = $temporada_folder . 'equipo_' . $equipo['name'] . '.xml';
        
        // Save the new DOMDocument to a file
        $equipo_xml->save($filename_equipo);
        
    }
}
?>

<?php

// Load the XML file
$xml = simplexml_load_file("data/temporadas.xml") or die("No se puede cargar el archivo");

// Create a new DOMDocument object
$dom = new DOMDocument('1.0');
$dom->preserveWhiteSpace = false;
$dom->formatOutput = true;

// Create root element
$users = $dom->createElement('users');
$dom->appendChild($users);

// Loop through each user in the original XML
foreach ($xml->users->user as $user) {
    // Extract user data
    $name = trim((string) $user->name);
    $email = trim((string) $user->email);
    $password = trim((string) $user->password);
    $role = trim((string) $user->role);

    // Create user element
    $userElement = $dom->createElement('user');
    $users->appendChild($userElement);

    // Add child elements
    $userElement->appendChild($dom->createElement('name', $name));
    $userElement->appendChild($dom->createElement('email', $email));
    $userElement->appendChild($dom->createElement('password', $password));
    $userElement->appendChild($dom->createElement('role', $role));
}

// Save the XML to a file
$dom->save('data/users/users.xml');

?>


