<?php
/*
// Check if the season and team parameters are provided
if(isset($_GET['season']) && isset($_GET['team'])) {
    // Load the XML file containing player data
    $xmlFile = '../../data/temporadas.xml';
    $xml = simplexml_load_file($xmlFile);

    // Get the selected team from the request
    $selectedTeam = $_GET['team'];

    // Array to store player data
    $players = [];

    // Iterate over teams in the XML file
    foreach ($xml->temporada->equipos->equipo as $equipo) {
        // Check if the team name matches the selected team
        if ((string)$equipo['name'] == $selectedTeam) {
            // Iterate over players in the team
            foreach ($equipo->jugadores->jugador as $jugador) {
                // Prepare player data
                $playerData = array(
                    "name" => (string)$jugador->name,
                    "imageSrc" => (string)$jugador->foto,
                    "altText" => "Photo of " . (string)$jugador->name 
                );

                // Add player data to array
                $players[] = $playerData;
            }
        }
    }

    // Output JSON response
    header('Content-Type: application/json');
    echo json_encode($players);
} else {
    // Output error message if parameters are missing
    http_response_code(400);
    echo "Error: Season or team parameter is missing";
}
*/

error_reporting(E_ALL);
ini_set('display_errors', 1);

// Check if the season and team parameters are provided
if(isset($_GET['season']) && isset($_GET['team'])) {
    // Get the selected season from the request
    $selectedSeason = $_GET['season'];

    // Load the XML file containing player data for the selected season
    $xmlFile = "../../data/seasons/{$selectedSeason}.xml";
    $xml = simplexml_load_file($xmlFile);

    // Get the selected team from the request
    $selectedTeam = $_GET['team'];

    // Array to store player data
    $players = [];

    // Iterate over teams in the XML file
    foreach ($xml->temporada->equipos->equipo as $equipo) {
        // Check if the team name matches the selected team
        if ((string)$equipo['name'] == $selectedTeam) {
            // Iterate over players in the team
            foreach ($equipo->jugadores->jugador as $jugador) {
                // Prepare player data
                $playerData = array(
                    "name" => (string)$jugador->name,
                    "imageSrc" => (string)$jugador->foto,
                    "altText" => "Photo of " . (string)$jugador->name 
                );

                // Add player data to array
                $players[] = $playerData;
            }
        }
    }

    // Output JSON response
    header('Content-Type: application/json');
    echo json_encode($players);
} else {
    // Output error message if parameters are missing
    http_response_code(400);
    echo "Error: Season or team parameter is missing";
}


?>
