<?php
if(isset($_GET["file"])) {
    $selectedFile = $_GET["file"];
    $xml = new DOMDocument();
    $xml->load($selectedFile);
    
    $xsl = new DOMDocument();
    $xsl->load('../../data/clasificacion/clasificacion.xsl'); 
    
    $proc = new XSLTProcessor();
    $proc->importStyleSheet($xsl);
    echo $proc->transformToXML($xml);
}
?>
