<?php
if (!isset($_SESSION)) session_start();

header('Content-Type: application/json');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $name = $_POST['name'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $subject = $_POST['subject'];
    $message = $_POST['message'];

    $xml = new SimpleXMLElement('<root/>');
    $xml->addChild('name', $name);
    $xml->addChild('phone', $phone);
    $xml->addChild('email', $email);
    $xml->addChild('subject', $subject);
    $xml->addChild('message', $message);

    $directory = '../../data/mensajes';
    if (!is_dir($directory)) {
        mkdir($directory, 0777, true);
    }

    $fileName = $directory . '/mensaje_' . time() . '.xml';
    $xml->asXML($fileName);

    echo json_encode(['status' => 'success', 'message' => 'Gracias por tu mensaje. Pronto enviaremos una respuesta a su correo electrónico.']);
    exit;
}
echo json_encode(['status' => 'error', 'message' => 'Se produjo un error al enviar el correo electrónico.']);
?>