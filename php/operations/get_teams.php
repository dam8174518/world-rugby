<?php

if(isset($_GET['season'])) {
   
    $xmlFile = '../../data/temporadas.xml';
    
   
    $xml = simplexml_load_file($xmlFile);


   
   
    $selectedSeason = $_GET['season'];

    
    $teams = [];
    foreach ($xml->temporada as $temporada) {
        if ((string)$temporada['id'] === $selectedSeason) {
            foreach ($temporada->equipos->equipo as $equipo) {
                $teams[] = (string)$equipo['name'];
                
            }
            break;
        }
    }

    
    header('Content-Type: application/json');
    echo json_encode($teams);


    
} else {
    
    http_response_code(400);
    echo "Error: Season parameter is missing";
}
?>
