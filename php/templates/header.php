<?php
if (!isset($_SESSION)) session_start();
?>
<header>
      <div class="header-container">
        <!--Contenedor que aparece solo en la ultima media query-->
        <div class="top-container"></div>
        <!--Seccion izquierda del header incluye el logo de la federacion-->
        <div class="header-left-section">
          <a href="index.php">
            <img
              class="world-rugby-logo"
              src="media/logos/world_rugby_logo.svg"
              alt="world rugby logo"
              width="50"
            />
          </a>
        </div>

        <!-- Menu de Navegacion -->
        <nav class="nav-container">
          <ul class="nav-list" id="nav-list">
            <li>
              <a class="nav-element" href="index.php">
                Inicio
                <span class="underline-principal"></span
              ></a>
            </li>
            <li>
              <a class="nav-element" href="noticias.php">
                Noticias
                <span class="underline-secundario"></span
              ></a>
            </li>
            <li>
              <a
                class="nav-element hiden-element-mobile"
                href="resultados.php"
                >Resultados <span class="underline-secundario"></span>
              </a>
            </li>
            <li>
              <a
                class="nav-element hiden-element-mobile"
                href="clasificacion.php"
              >
                Clasificación <span class="underline-secundario"></span>
              </a>
            </li>
            <li>
              <a
                class="nav-element hiden-element-mobile"
                href="jugadores.php"
              >
                Jugadores
                <span class="underline-secundario"></span>
              </a>
            </li>
          </ul>
        </nav>
        <!--Sección derecha del header incluye el cuadro de búsqueda 
          y el botón que permite cambiar el idioma, también el inicio de sesión-->
        <div class="header-right-section">
          <input
            class="search-input"
            id="search-input"
            type="text"
            placeholder="Search"
            maxlength="200"
          />
          <!-- el cuadro de busqueda-->
          <button
            class="search-button"
            id="search-button"
            onclick="toggleInputSearch()"
          >
            <img
              class="search-icon"
              id="search-icon"
              src="media/icons/search_icon.svg"
              alt="search-icon"
            />
          </button>
          <!--El buton que permite cambiar el idioma contiene 
            funciones que encargan este caso-->
          <div class="language-button" onclick="toggleLanguageList()">
            <!--icono de idiomas para el ordenador-->
            <img
              class="globe-icon-desktop"
              src="media/icons/globe_icon.png"
              alt="globe-icon-desktop"
            />
            <!--icono de idiomas para el movil-->
            <img
              src="media/icons/glob_icon_mobile.png"
              alt="globe-icon-mobile"
              class="globe-icon-mobile"
            />
            <!--función que cambia el texto basando en el idioma elegido-->
            <span id="selected-language" class="selected-language"
              >Español</span
            >
            <div class="language-list" id="language-list">
              <!--<a href="#" onclick="selectLanguage('English')">English </a> -->
              <a href="#" onclick="selectLanguage('Español')">Español </a>
            </div>
          </div>

          
            <?php 

            if (isset($_SESSION['username'])) {
               
                echo '  
                <a href="user-account.php">    
               <button class="sign-in-button"> ' . $_SESSION['username']  . '</button>    
               </a>';
           
              } else {   
                echo '
                <a href="login-form.php">
                <button class="sign-in-button">Iniciar Sesion</button>
                </a>';
            }    
            ?>
        </div>
      </div>
      <!--icono de menú desplegable cerrado; aparece solo en versión del móvil-->
      <img
        class="list-menu-icon"
        src="media/icons/list_icon.png"
        alt="list-menu-icon"
        id="list-menu-icon"
        onclick="toggleMenuList()"
      />
      <!--icono de menú desplegable abierto; aparece solo en versión del móvil-->
      <img
        src="media/icons/close_menu_icon.svg"
        alt="close-menu-icon"
        class="close-menu-icon"
        id="close-menu-icon"
        onclick="toggleCloseMenuList()"
      />
      <hr class="header-underline" />


    </header>